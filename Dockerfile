FROM  openjdk:8-jdk-alpine
VOLUME /tmp
EXPOSE 9090
ADD target/oauth-server-0.0.1-SNAPSHOT.jar oauth-server.jar
RUN  sh -c  'touch /oauth-server.jar'
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/oauth-server.jar"]