package com.demo.shared;


import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Validate {


    public   boolean emailIsvalid(String userEmail){
        Pattern pattern  = Pattern.compile("^[^@]+@[^@]+\\.[a-zA-Z]{2,}$");
        Matcher matcher = pattern.matcher(userEmail);
        return  matcher.find();
    }

     public boolean isvalidPassWord(String pasword){

        Pattern pattern  =
                Pattern.compile("^(?=.*\\d)(?=.*[\\u0021-\\u002b\\u003c-\\u0040])(?=.*[A-Z])(?=.*[a-z])\\S{8,16}$");
        Matcher  matcher = pattern.matcher(pasword);
        return  matcher.find();
    }
}
