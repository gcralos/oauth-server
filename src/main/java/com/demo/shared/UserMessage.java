package com.demo.shared;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class UserMessage {

    public Map<String, String>  message(String statusCode, String message){
        HashMap<String,String>  messageMap =  new HashMap<>();
        messageMap.put("statusCode", statusCode);
        messageMap.put("message",message);
        return messageMap;
    }
}
