package com.demo.shared;

import org.springframework.stereotype.Component;


public class EndPoint {

    public static final String USUARIO_ID="/user/{id}";
    public static final String SIGN_UP_URL="/oauth/autorizacion";
    public static final String EMAIL_VALID="/validar";


}
