package com.demo.shared.redis.codigovalidacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CodigoValidacionServices implements CacheRedis<CodigoVerificacion> {

    @Autowired
    private CodigoValidacionRepositorio codigoValidacionRepositorio;


    @Override
    public CodigoVerificacion add(boolean b, String s) {
        CodigoVerificacion codigoVerificacion =  new CodigoVerificacion();
        codigoVerificacion.setSendVerify(b);
        codigoVerificacion.setUsername(s);
        return  codigoValidacionRepositorio.save(codigoVerificacion);

    }

    @Override
    public boolean findById(String id) {

        try{
            Optional<CodigoVerificacion> optiona =
                        Optional.of(codigoValidacionRepositorio.findById(id).get());
            System.out.printf(String.valueOf(optiona.get().getId()));
            return optiona.isPresent();

        }catch ( Exception e){
            System.out.printf("error" +e.getMessage());
            return  false;
        }
    }

    @Override
    public void findBydAll(List<CodigoVerificacion> codigoValidacion) {
        List<CodigoVerificacion>  codigoValidacions = new ArrayList<>();
        codigoValidacionRepositorio.findAll().forEach(codigoValidacions::add);
    }

    @Override
    public void update(CodigoVerificacion codigoValidacion) {
        codigoValidacionRepositorio.save(codigoValidacion);
    }

    @Override
    public void delete(CodigoVerificacion id) {
        codigoValidacionRepositorio.deleteById(id.getId());
    }


}
