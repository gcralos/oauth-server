package com.demo.shared.redis.codigovalidacion;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CodigoValidacionRepositorio  extends CrudRepository<CodigoVerificacion,String> {
    Optional<CodigoVerificacion> findById( String  username);
    Optional<CodigoVerificacion> findByCodigo(String  codigo);
}
