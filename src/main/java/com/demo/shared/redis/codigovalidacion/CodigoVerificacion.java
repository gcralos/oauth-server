package com.demo.shared.redis.codigovalidacion;


import org.springframework.data.redis.core.RedisHash;

@RedisHash("CodigoValidacion")
public class CodigoVerificacion {

    private String  id;
    private boolean  sendVerify;
    private String  username;


    public CodigoVerificacion() {
    }

    public boolean getSendVerify() {
        return sendVerify;
    }

    public void setSendVerify(boolean sendVerify) {
        this.sendVerify = sendVerify;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
