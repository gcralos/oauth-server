package com.demo.shared.redis.codigovalidacion;

import java.util.List;

public interface CacheRedis<T> {
    T add( boolean  b, String  s);
    boolean findById(String  id);
    void findBydAll(List<T> t);
    void update( T t);
    void delete(T t);

}
