package com.demo.managementtoken.infraestruture;

import static com.demo.shared.Constans.HEADER_STRING;
import static com.demo.shared.Constans.TOKEN_PREFIX;
import static com.demo.shared.Constans.SECRET;
import static com.demo.shared.Constans.EXPIRATION_TIME;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.demo.managementtoken.aplicaction.OauthServicio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.demo.user.domain.UserEntidad;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;

    @Autowired
    public OauthServicio oauthServicio;


    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;

    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse response) throws AuthenticationException {

        try {

            UserEntidad userEntity = new ObjectMapper()
                    .readValue(req.getInputStream(), UserEntidad.class);
            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            userEntity.getUserName(),
                            userEntity.getPassword(),
                            new ArrayList<>())
            );
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {
        String token = Jwts.builder()
                .setSubject(((User) auth.getPrincipal()).getUsername())
                .claim("IdRol", ((User) auth.getPrincipal()).getAuthorities())
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();

        Map<String, String> map = new HashMap<>();
        map.put("acces_token", token);
        map.put("type", "Bearer");
        map.put("exp","");
        ObjectMapper mapper = new ObjectMapper();
        String value = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map);
        res.getWriter().write(value);
        res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);

    }

}
