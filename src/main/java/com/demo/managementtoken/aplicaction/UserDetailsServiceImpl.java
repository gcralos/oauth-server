package com.demo.managementtoken.aplicaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.security.core.GrantedAuthority;
import com.demo.user.domain.UserEntidad;
import com.demo.user.domain.UserRepositorio;


import org.springframework.security.core.userdetails.User;


import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepositorio userRepositorio;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        Optional<UserEntidad> userEntity = userRepositorio.findByUserName(userName);

               if( userEntity.get().isStatusEmail() == equals(false)){

                   throw new UsernameNotFoundException(userName);
               } else if (userEntity == null){
                   throw new UsernameNotFoundException(userName);
               }
                Set<GrantedAuthority> grantedAuthority = new HashSet<>();
                return new User(userEntity.get().getUserName(), userEntity.get().getPassword(), grantedAuthority);

    }
}

