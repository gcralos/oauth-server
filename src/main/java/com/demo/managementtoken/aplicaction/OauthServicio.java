package com.demo.managementtoken.aplicaction;

import com.demo.managementtoken.domain.OauthTokenEntidad;
import com.demo.managementtoken.domain.OauthToken;
import com.demo.shared.UserMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

import static com.demo.shared.Message.MESSAGE_USER_VALID;

@Service
public class OauthServicio {

    @Autowired
    private OauthToken logoutRepositorio;
    private OauthTokenEntidad logoutEntidad;
    private UserMessage  userMessage;
    private String  userName;
    private String  token;


    public ResponseEntity<Object>  add(){
        LocalDate localDate = LocalDate.now();
        Optional<OauthTokenEntidad>  optional = logoutRepositorio.findByTokenUser(logoutEntidad.getTokenUser());
        optional.get().setStatusToken("Activo");

        return ResponseEntity.status(201)
                .body(userMessage.message("201",MESSAGE_USER_VALID));
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
