package com.demo.managementtoken.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OauthToken extends JpaRepository <OauthTokenEntidad, Long > {

    Optional<OauthTokenEntidad>  findByTokenUser(String  tokenUser  );

}
