package com.demo.managementtoken.domain;



import com.demo.user.domain.UserEntidad;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.lang.NonNull;

import javax.persistence.*;

@Entity
@Table(name="oauth_token")
public class OauthTokenEntidad {

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO)
    private long  id;

    @Column(name="ip_remote_addres", length = 20)
    private String  ipRemoteAddres;

    @Column(name="token_user", length = 300)
    private String  tokenUser;

    @Column(name="type_token", length = 20)
    private String  type_token;

    @Column(name="status_token", length = 20)
    private String  statusToken;

    @NonNull
    @Column(length = 100)
    private String createBy;

    @NonNull
    @Column()
    @JsonFormat(pattern = "yyyy-mm-dd", shape = JsonFormat.Shape.STRING)
    private String createDt;


    @Column(length = 100)
    private String updateBy;

    @Column()
    @JsonFormat(pattern = "yyyy-mm-dd", shape = JsonFormat.Shape.STRING)
    private String updateDt;


    @ManyToOne
    @JoinColumn(name = "fk_id_user")
    private  UserEntidad userEntidad;

    public OauthTokenEntidad(String ipRemoteAddres, String tokenUser, String type_token, String statusToken, @NonNull String createBy, @NonNull String createDt, String updateBy, String updateDt) {
        this.ipRemoteAddres = ipRemoteAddres;
        this.tokenUser = tokenUser;
        this.type_token = type_token;
        this.statusToken = statusToken;
        this.createBy = createBy;
        this.createDt = createDt;
        this.updateBy = updateBy;
        this.updateDt = updateDt;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIpRemoteAddres() {
        return ipRemoteAddres;
    }

    public void setIpRemoteAddres(String ipRemoteAddres) {
        this.ipRemoteAddres = ipRemoteAddres;
    }

    public String getTokenUser() {
        return tokenUser;
    }

    public void setTokenUser(String tokenUser) {
        this.tokenUser = tokenUser;
    }

    public String getType_token() {
        return type_token;
    }

    public void setType_token(String type_token) {
        this.type_token = type_token;
    }

    public String getStatusToken() {
        return statusToken;
    }

    public void setStatusToken(String statusToken) {
        this.statusToken = statusToken;
    }

    @NonNull
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(@NonNull String createBy) {
        this.createBy = createBy;
    }

    @NonNull
    public String getCreateDt() {
        return createDt;
    }

    public void setCreateDt(@NonNull String createDt) {
        this.createDt = createDt;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateDt() {
        return updateDt;
    }

    public void setUpdateDt(String updateDt) {
        this.updateDt = updateDt;
    }

    public UserEntidad getUserEntidad() {
        return userEntidad;
    }

    public void setUserEntidad(UserEntidad userEntidad) {
        this.userEntidad = userEntidad;
    }
}
