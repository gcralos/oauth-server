package com.demo.user.infraestructure.controller;

import com.demo.shared.redis.codigovalidacion.CodigoVerificacion;
import com.demo.user.aplication.Delete;
import com.demo.user.aplication.Save;
import com.demo.user.aplication.Update;
import com.demo.user.domain.UserModelo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.demo.shared.EndPoint.*;

@RestController
public class UserController {

    @Autowired
    private Update update;
    @Autowired
    private Delete deleteAplication;
    @Autowired
    private Save save;

    public ResponseEntity hola()throws  Exception{
        return  ResponseEntity.status(200).body("e");
    }

    @PostMapping(path = "/oauth/autorizacion")
    public ResponseEntity<Object> getUsuario(@RequestBody UserModelo userModelo) throws Exception {
        return save.add(userModelo);
    }

    @PutMapping(EMAIL_VALID)
    public ResponseEntity<Object> updateUsuario(@RequestBody CodigoVerificacion codigoVerificacion) {
        return update.update(codigoVerificacion);
    }
    @PutMapping(EMAIL_VALID+"/{id}")
    public ResponseEntity<Object> updateUsuario(@RequestBody UserModelo usuario) {

        return update.update(usuario);
    }

    @DeleteMapping(USUARIO_ID)
    public void deleteUsuario(@PathVariable Long id) {
        deleteAplication.delete(id);
    }

}
