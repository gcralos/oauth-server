package com.demo.user.domain;

import javax.persistence.*;

import com.demo.managementtoken.domain.OauthTokenEntidad;
import org.springframework.lang.NonNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

import java.util.List;

@Entity
@Table(name = "users")
public class UserEntidad {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NonNull
    @Column(length = 100, unique = true)
    private String userName;

    @NonNull
    @Column(length = 100)
    private String password;

    @NonNull
    @Column(length = 100)
    private String createBy;

    @NonNull
    @Column(name="status_email")
    private  boolean  statusEmail;

    @NonNull
    @Column()
    @JsonFormat(pattern = "yyyy-mm-dd", shape = Shape.STRING)
    private String createDt;


    @Column(length = 100)
    private String updateBy;

    @Column()
    @JsonFormat(pattern = "yyyy-mm-dd", shape = Shape.STRING)
    private String updateDt;

    @OneToMany( mappedBy = "userEntidad")
    private List<OauthTokenEntidad> logoutEntidad;



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateDt() {
        return createDt;
    }

    public void setCreateDt(String createDt) {
        this.createDt = createDt;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateDt() {
        return updateDt;
    }

    public void setUpdateDt(String updateDt) {
        this.updateDt = updateDt;
    }

    public List<OauthTokenEntidad> getLogoutEntidad() {
        return logoutEntidad;
    }

    public void setLogoutEntidad(List<OauthTokenEntidad> logoutEntidad) {
        this.logoutEntidad = logoutEntidad;
    }

    public boolean isStatusEmail() {
        return statusEmail;
    }

    public void setStatusEmail(boolean statusEmail) {
        this.statusEmail = statusEmail;
    }
}
