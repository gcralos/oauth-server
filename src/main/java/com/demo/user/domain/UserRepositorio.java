package com.demo.user.domain;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;


@Repository
public interface UserRepositorio extends JpaRepository<UserEntidad, Long> {

    Optional<UserEntidad> findByUserName(String userName);

    Optional<UserEntidad> findById(Long id);

    @Transactional
    @Modifying
    @Query(value="update users set status_email= true where  user_name = :userNam",nativeQuery = true)
    int update( @Param("userNam") String userNam);

    @Transactional
    @Modifying
    @Query(value="select status_email from users  where  user_name = :userNam",nativeQuery = true)
    int emailVerify( @Param("userNam") String userNam);


}
