package com.demo.user.aplication;

import com.demo.user.domain.UserRepositorio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Delete {

    private static final Logger logger = LogManager.getLogger(Delete.class);
    @Autowired
    private UserRepositorio userRepositorio;

    public void delete(Long id) {
        userRepositorio.deleteById(id);
    }
}
