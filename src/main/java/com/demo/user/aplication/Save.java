package com.demo.user.aplication;

import com.demo.shared.redis.RedisConfiguration;
import com.demo.shared.redis.codigovalidacion.CodigoValidacionServices;
import com.demo.shared.Kafka.KafkaMessageProducer;
import com.demo.shared.UserMessage;
import com.demo.shared.Validate;
import com.demo.user.domain.UserEntidad;
import com.demo.user.domain.UserModelo;
import com.demo.user.domain.UserRepositorio;
import com.google.gson.Gson;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.time.LocalDate;
import java.util.Optional;

import static com.demo.shared.Constans.*;
import static com.demo.shared.Message.*;

@Service
public class Save {


    @Autowired
    private UserRepositorio userRepositorio;
    @Autowired
    private CodigoValidacionServices codigoValidacionServices;
    @Autowired
    private UserMessage userMessage;
    @Autowired
    KafkaMessageProducer kafkaMessageProducer;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private RedisConfiguration redisConfiguration;

    private static final Logger logger = LoggerFactory.getLogger(Save.class);
    ModelMapper mapperModel = new ModelMapper();
    Validate validate = new Validate();
    Gson gson = new Gson();
    LocalDate localDate = LocalDate.now();


    public ResponseEntity<Object> add(UserModelo userModelo) {
        long startTime = System.nanoTime();
        logger.error(String.valueOf(startTime));

        Optional<UserEntidad> optionalUserEntidad = userRepositorio.findByUserName(userModelo.getUserName());
        if (optionalUserEntidad.isPresent())
            return ResponseEntity.status(400)
                    .body(userMessage.message("400", MESSAGE_EMAIL_MACHERT));
        if (!validate.emailIsvalid(userModelo.getUserName()))
            return ResponseEntity.status(400)
                    .body(userMessage.message("400", MESSAGE_EMAIL_INVALID));
        try {
            userModelo.setPassword(bCryptPasswordEncoder.encode(userModelo.getPassword()));
            userModelo.setCreateBy(userModelo.getUserName());
            userModelo.setCreateDt(localDate.toString());
            userModelo.setUpdateBy(userModelo.getUserName());
            userModelo.setUpdateDt(localDate.toString());
            UserEntidad userEntidad = mapperModel.map(userModelo, UserEntidad.class);
            String json = gson.toJson(codigoValidacionServices.add(false, userModelo.getUserName()));
            kafkaMessageProducer.sendMessage("registro", json);
            userRepositorio.save(userEntidad);
            return ResponseEntity.status(200).body(userMessage.message("200",USER_SUCESS));
        }catch (Exception e){
             return  ResponseEntity.status(400).body(ERROR_REGISTRO);
        }
    }
}