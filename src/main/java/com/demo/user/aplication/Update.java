package com.demo.user.aplication;

import com.demo.shared.redis.codigovalidacion.CodigoValidacionServices;
import com.demo.shared.redis.codigovalidacion.CodigoVerificacion;
import com.demo.user.domain.UserEntidad;
import com.demo.user.domain.UserModelo;
import com.demo.user.domain.UserRepositorio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class Update {
    private static final Logger logger = LogManager.getLogger(Update.class);
    @Autowired
    private UserRepositorio userRepositorio;
    private UserEntidad userModel;
    @Autowired
    private CodigoValidacionServices codigoValidacionServices;

    public ResponseEntity<Object> update(UserModelo user) {

        try {
            Optional<UserEntidad> optional = userRepositorio.findById(user.getId());
            if(optional.isPresent())
                userRepositorio.update(user.getUserName());
            return ResponseEntity.status(201).body("hola");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e);
        }

    }
    public ResponseEntity<Object> update(CodigoVerificacion codigoVerificacion) {

        try {

            if(codigoValidacionServices.findById(codigoVerificacion.getId()))
                userRepositorio.update(codigoVerificacion.getUsername());
            return ResponseEntity.status(201).body("hola");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e);
        }

    }

}
