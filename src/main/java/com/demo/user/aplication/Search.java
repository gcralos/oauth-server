package com.demo.user.aplication;

import com.demo.user.domain.UserEntidad;
import com.demo.user.domain.UserRepositorio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Search {

    private static final Logger logger = LogManager.getLogger(Search.class);
    @Autowired
    private UserRepositorio userRepositorio;
    private UserEntidad userModel;

    public Object getId(Long id) {

        return   userRepositorio.findById(id).get().isStatusEmail();
    }
}
