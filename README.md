<h1>Autenticación y  Autorización  con JWT</h1>
Autenticación  de usuario utilizando  spring  boot   y spring segurity
Introducción

En este documento se trata el diseño de la autenticación de usuario utilizando spring boot y spring segurity con integración con Redis y apache Kafka

<h4>Apache Kafka</h4>

Apache Kafka es una plataforma distribuida de transmisión de datos que permite publicar, almacenar y procesar flujos de registros, y suscribirse a ellos, en tiempo real. Está diseñada para manejar flujos de datos de varias fuentes y distribuirlos a los diversos usuarios. En resumen, transfiere cantidades enormes de datos, no solo desde el punto A hasta el B, sino también del punto A al Z y a cualquier otro lugar que necesite, y todo al mismo tiempo.

<h4>Ventajas</h4> 
•	Buena escalabilidad</br>
•	Tolerancia a fallos</br>
•	Plataforma de streaming de eventos</br>
•	Multi-tenant</br>
•	Capaz de procesar en tiempo real</br>
•	Enfocado a proyectos Big Data</br>
<h4>Desventajas</h4>
•	Dependencia con Apache Zookeeper</br>
•	Sin enrutamiento de mensajes</br>
•	Carece de componentes de monitorización</br>

Apache Kafka combina los dos patrones de mensajería descritos anteriormente, las colas de mensajes con publicador-suscriptor, obteniendo las ventajas de ambos. Kafka también se encarga de garantizar el orden de los mensajes para cada consumidor.
<img src="./img/imguno.png">

<h1>Redis</h1>
Redis es un almacén de estructura de datos en memoria de código abierto (licencia BSD), que se utiliza como agente de base de datos, caché y mensaje. Admite estructuras de datos como cadenas, hashes, listas, conjuntos, conjuntos ordenados con consultas de rango, mapas de bits, hiperloglogs, índices geoespaciales con consultas de radio y flujos. Redis tiene replicación incorporada, secuencias de comandos Lua, desalojo de LRU, transacciones y diferentes niveles de persistencia en el disco, y proporciona alta disponibilidad a través de Redis Sentinel y particionamiento automático con Redis Cluster.

<h4>Ventaja</h4>
Una velocidad muy por encima de la media respecto a otras DB de su tipo, gracias a su almacenamiento en memoria</br>
Posibilidad de persistir datos en disco para recuperación ante fallas</br>
Fácil configuración</br>
Alta disponibilidad</br>
Curva de aprendizaje baja</br>
Extensible usando LUA scripting<br>
Una variedad de tipos de datos<br>

<h4>Desventajas</h4>
El método de persistencia RDB consume mucho I/O (escritura en disco)</br>
Todos los datos trabajados deben encajar en la memoria (en caso de no usar persistencia física)

<h1>JSON Web Tokens</h1>
JSON Web Token (JWT) es un estándar abierto ( RFC 7519 ) que define una forma compacta y autónoma para transmitir información de forma segura entre las partes como un objeto JSON. Esta información se puede verificar y confiar porque está firmada digitalmente. Los JWT se pueden firmar usando un secreto (con el algoritmo HMAC ) o un par de claves pública / privada usando RSA o ECDSA .

<h4>Estructura JSON Web Token</h1>

En su forma compacta, JSON Web Tokens consta de tres partes separadas por puntos ( .), que son:
Encabezamiento,
Carga útil,
Firma.

Por lo tanto, un JWT generalmente se parece a lo siguiente.
xxxxx.yyyyy.zzzzz.<br>

<h4>Encabezamiento</h4>
El encabezado generalmente consta de dos partes: el tipo de token, que es JWT, y el algoritmo de firma que se utiliza, como HMAC SHA256 o RSA.</br>
<h4>Carga útil</h4>
La segunda parte del token es la carga útil, que contiene los reclamos. Las reclamaciones son declaraciones sobre una entidad (típicamente, el usuario) y datos adicionales. Hay tres tipos de reclamos: reclamos registrados, públicos y privados.<br>
<h4>Firma</h4>
Para crear la parte de firma, debe tomar el encabezado codificado, la carga útil codificada, un secreto, el algoritmo especificado en el encabezado y firmarlo.<br>

<h1>Alternativas</h1>
Las alternativas  que pueden surgir a estas tecnologías</br>
Apache Kafka  podría ser remplazado  por   RabbitMQ.</br>
Para  Redis  Apache  Kasandra,  mongoDB.</br>
Para  JWT  Cokis,sesiones.

<h1>El caso de uso seria el siguiente</h1>
<i>Se requiere  que desde un portal web  o un dispositivo  móvil se   pueda  autenticar  y autorizar  un  usuario  para el ingreso  a la plataforma. Utilizando el email  y password.</i><br>

<h5>Un flujo típico que se puede tomar en este caso es  el siguiente:</h5>
1)El usuario  ingresa  el email  y pasword.</br>
2 El sistema  valida que el formato de email  y password  sean validos.</br>
3) Si son validos  el sistema guarda la información del usuario en la base de datos.</br>
4)El sistema envía  un correo  con  un código de validación.</br>
5
5)El usuario  consulta el correo    e ingresa  el código en el aplicativo.</br>
6)El  usuario queda   verificado  y    registrado.</br>
<h5>Flujo  alterno  1 </h5>
El usuario   ingresa   el password  o email    equivocados  o  ya existe un  usuario con ese mismo correo   el sistema mostrara un   mensaje  el usuario ya existe  o  credenciales no validas .</br>
<h5>Flujo  alterno  2</h5>
 En caso que el usuario no reciba el correo  pude  volver  a reenviarlo  accionando un botón<br>

<h1> Diagrama de flujo</h1>
<img src="./img/Diagrama en blanco.jpeg">

<h1>La estructura de componentes </h2>
<br>
<img src="./img/Digrama_componetes.png">
